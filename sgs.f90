module sgs
use def
use fft

contains
  subroutine eddy_viscosity(uh,vh,vt,bxc,byc,bxp,byp,ik,il)
  complex, dimension(bs), intent(inout) :: uh,vh,vt
  complex, dimension(bs), intent(inout) :: bxc,byc,bxp,byp
  complex, dimension(bs), intent(in) :: ik,il

  complex, dimension(bs) :: sxxh,sxyh,syyh,sxx,sxy,syy
  complex, dimension(bs) :: tau_xxh,tau_xyh,tau_yyh,tau_xx,tau_xy,tau_yy
  integer :: i
  
  do i=1,bs
    sxxh(i) = ik(i)*uh(i)
    sxyh(i) = 0.5*(il(i)*uh(i)+ik(i)*vh(i))
    syyh(i) = il(i)*vh(i)
  enddo

  call ifft2(sxxh,sxx)
  call ifft2(sxyh,sxy)
  call ifft2(syyh,syy)

  do i=1,bs
    vt(i) = cs2*sqrt(2*(sxx(i)**2+2*sxy(i)**2+syy(i)**2))
  enddo

  do i=1,bs
    tau_xx(i) = -2*vt(i)*sxx(i)
    tau_xy(i) = -2*vt(i)*sxy(i)
    tau_yy(i) = -2*vt(i)*syy(i)
  enddo

  call fft2(tau_xx,tau_xxh)
  call fft2(tau_xy,tau_xyh)
  call fft2(tau_yy,tau_yyh)
  
  do i=1,bs
    bxc(i) = ik(i)*tau_xxh(i)+il(i)*tau_xyh(i)
    byc(i) = ik(i)*tau_xyh(i)+il(i)*tau_yyh(i)
  enddo
  
  if (tt==1) then
    uh = uh - dt*bxc
    vh = vh - dt*byc
  else
    uh = uh - (dtl*bxc - dts*bxp)
    vh = vh - (dtl*byc - dts*byp)
  endif  

  bxp = bxc; byp = byc
  end subroutine eddy_viscosity


  subroutine diffusion(uh,vh,d)
  complex, dimension(bs), intent(inout) :: uh,vh
  real, dimension(bs), intent(in) :: d

  integer :: i

  do i=1,bs
    uh(i) = d(i)*uh(i)
    vh(i) = d(i)*vh(i)
  enddo
  end subroutine diffusion

end module sgs

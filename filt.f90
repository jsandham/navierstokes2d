module filt
use def


contains
  subroutine cut_off(uh,vh,ik,il)
  complex, dimension(bs), intent(inout) :: uh,vh
  complex, dimension(bs), intent(in) :: ik,il

  integer :: i,j

  do i=1,n
    if (abs(ik(i))>=kc) then
      uh(i) = 0.0
      vh(i) = 0.0
    elseif (abs(il(i))>=kc) then
      uh(i) = 0.0
      vh(i) = 0.0
    endif
  enddo
  end subroutine cut_off




  subroutine box(uh,vh)


  end subroutine box





  subroutine gaussian(uh,vh,ik,il)
  complex, dimension(bs), intent(inout) :: uh,vh
  complex, dimension(bs), intent(in) :: ik,il

  integer :: i,j

  do i=1,bs
    uh(i) = uh(i)*exp(-2.0*dx**2*(abs(ik(i))**2+abs(il(i))**2))
    vh(i) = vh(i)*exp(-2.0*dx**2*(abs(ik(i))**2+abs(il(i))**2))
  enddo

  end subroutine gaussian


end module filt


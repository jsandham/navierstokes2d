!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NavierStokes2D 
! main.f90 program
! James Sandham 
! 30 Jan 2014
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program main
use mpi
use def
use init
use advec
use sgs
use press
use filt
use fft
use util
use output

call MPI_INIT(ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierr)

call init_fftw_plans()

if (mod(n,nproc)/=0) then
   print *, '***Number of processors must divide evenly into n***'
   stop
elseif (bs.ne.n*n/nproc) then
   print *, '***block size is not correct***'
   stop
elseif (rank==root) then
   print *, '*****************************************************'
   print *, '* 2D Parallel Navier Stokes'
   print *, '* written by James Sandham'
   print *, '* 20 Mar 2014'
   print *, '*****************************************************' 
   print *, '* N = ',n
   print *, '* dt = ',dt
   print *, '* Re = ',Re
   print *, '* Hyperviscosity = ',h
   print *, '* # of timesteps = ',nt
   print *, '* Number of processors: ',nproc
   print *, '* Run type: ',run_type
   print *, '* Plotting frequency = ',plot_freq
   print *, '*****************************************************'
   call cpu_time(tstart)
endif

call initialization(u,v,vt,fx,fy,ik,il,k2,x,y,p1,p2,p3,d)  
call MPI_BARRIER(MPI_COMM_WORLD,ierr)

call fft2(u,u) ; call mpitranspose(u)
call fft2(v,v) ; call mpitranspose(v)
call fft2(fx,fx) ; call mpitranspose(fx)
call fft2(fy,fy) ; call mpitranspose(fy)

do ind=1,bs
   u(ind) = c*u(ind)*k2(ind)**2*exp(-(pi/16)*k2(ind))
   v(ind) = c*v(ind)*k2(ind)**2*exp(-(pi/16)*k2(ind))
enddo
call pressure_proj(u,v,p1,p2,p3)


do tt=1,nt
  
  call advection(u,v,ascr1,ascr2,ascr3,ascr4,fx,fy,ik,il)
  if (run_type=='LES') then
     call eddy_viscosity(u,v,vt,bscr5,bscr6,bscr7,bscr8,ik,il)
  elseif (run_type=='DNS') then
     call diffusion(u,v,d)
  endif
  call pressure_proj(u,v,p1,p2,p3)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

  if (rank==root .and. mod(tt,plot_freq)==0) then
     print *,' Timestep # ',tt,' Model time: ', dt*tt,'u: ',u(1)
  endif
 
enddo


call vorticity(u,v,omega,ik,il)
call create_netcdf_file(real(omega))

if (rank==root) then
   call cpu_time(tfinish)
   telapsed = tfinish-tstart
   print *, '***Total Elapsed Time***: ',telapsed
endif

call dest_fftw_plans()
call MPI_FINALIZE(ierr)
end program main


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NavierStokes2D 
! util.f90 module
! James Sandham 
! 4 Jan 2014
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module util
use def
use fft

contains 

   !subroutine display_array(input)
   !complex, dimension(n,n), intent(in) :: input

   !integer :: row, col
   !do row=1,n
   !   write(*,1) (real(input(row,col)),col=1,n)
   !enddo
   !1 format(10f20.4)
   !end subroutine display_array


   subroutine vorticity(uh,vh,omega,ik,il)
   complex, dimension(bs), intent(inout) :: omega
   complex, dimension(bs), intent(in) :: uh, vh, ik, il

   integer :: i
   complex, dimension(bs) :: omegah

   do i=1,bs
      omegah(i) = il(i)*uh(i)-ik(i)*vh(i)
   enddo

   call ifft2(omegah,omega)
   call mpitranspose(omega)
   end subroutine vorticity   

end module util

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NavierStokes2D 
! init.f90 module
! James Sandham 
! 30 Jan 2014
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module init
use def

  complex, dimension(bs) :: u=0.0, v=0.0, vt=0.0, fx=0.0, fy=0.0, omega=0.0
  complex, dimension(bs) :: ascr1=0.0, ascr2=0.0, ascr3=0.0, ascr4=0.0, bscr5=0.0, bscr6=0.0, bscr7=0.0, bscr8=0.0
  complex, dimension(bs) :: ik=0.0, il=0.0 
  real, dimension(bs) :: x=0.0, y=0.0
  real, dimension(bs) :: p1=0.0, p2=0.0, p3=0.0
  real, dimension(bs) :: d=0.0, k2=0.0
 

contains
  subroutine initialization(u,v,vt,fx,fy,ik,il,k2,x,y,p1,p2,p3,d)
  complex, dimension(bs), intent(inout) :: u, v, vt, fx, fy
  complex, dimension(bs), intent(inout) :: ik, il 
  real, dimension(bs), intent(inout) :: x, y
  real, dimension(bs), intent(inout) :: p1, p2, p3
  real, dimension(bs), intent(inout) :: d, k2

  integer :: i,j
  integer, dimension(12) :: seed
  real :: r1,r2

  real, dimension(n) :: kk
  real, dimension(n) :: ll
  real, dimension(n) :: xx
  real, dimension(n) :: yy
  real, dimension(n) :: ones=1.0
  real, dimension(bs) :: k, l

  seed(:) = int(86345*rank+23456)
  call random_seed(put=seed)
  
  u=0.0; v=0.0; vt=0.0

  do i=1,bs
    call random_number(r1) ; call random_number(r2)
    u(i) = 2*r1-1
    v(i) = 2*r2-1
  enddo

  kk = wave_num(n)
  ll = wave_num(n)

  do i=1,bs/n
    k(1+n*(i-1):n*i)=kk
    l(1+n*(i-1):n*i)=ll(bs*rank/n+i)*ones
  enddo

  ik = ii*k
  il = ii*l
  k2 = k**2+l**2 

  xx = xgrid(n)
  yy = ygrid(n)

  do i=1,bs/n
    x(1+n*(i-1):n*i)=xx
    y(1+n*(i-1):n*i)=yy(bs*rank/n+i)*ones
  enddo

  !fx = (2.0 + tanh(y-pi))*exp(-4*((x-pi)**2+(y-pi)**2))
  !fy = 0.0*y
  
  do i=1,bs
    if (rank==root .and. i==1) then
      p1(i) = 0.0
      p2(i) = 0.0
      p3(i) = 0.0
    else
      p1(i) = l(i)**2/(k(i)**2+l(i)**2)
      p2(i) = -l(i)*k(i)/(K(i)**2+l(i)**2)
      p3(i) = k(i)**2/(k(i)**2+l(i)**2)
    endif
  enddo

  do i=1,bs
     d(i) = (1-theta*(dt/Re)*(k(i)**2+l(i)**2)**hyper)/(1+(1-theta)*(dt/Re)*(k(i)**2+l(i)**2)**hyper)
  enddo
  end subroutine initialization

  
  function wave_num(s)
  integer :: s,i
  real, dimension(s) :: wave_num
  do i=1,s
     if (i<=s/2) then
        wave_num(i)=i-1
     else
        wave_num(i)=-(s-i+1)
     end if
  end do
  end function wave_num


  function xgrid(s)
  integer :: s,i
  real, dimension(s) :: xgrid
  do i=1,s
     xgrid(i)=i*dx
  end do
  end function xgrid


  function ygrid(s)
  integer :: s,i
  real, dimension(s) :: ygrid
  do i=1,s
     ygrid(i)=i*dy
  end do
  end function ygrid

end module init

module advec
use def
use fft

contains
  subroutine advection(uh,vh,axc,ayc,axp,ayp,fxh,fyh,ik,il)
  complex, dimension(bs), intent(inout) :: uh,vh
  complex, dimension(bs), intent(inout) :: axc,ayc,axp,ayp
  complex, dimension(bs), intent(in) :: fxh,fyh
  complex, dimension(bs), intent(in) :: ik,il
  
  complex, dimension(bs) :: ur,vr
  complex, dimension(bs) :: uur,uvr,vvr
  complex, dimension(bs) :: uuh,uvh,vvh
  integer :: i

  call ifft2(uh,ur)
  call ifft2(vh,vr)

  do i=1,bs
    uur(i) = ur(i)*ur(i)
    uvr(i) = ur(i)*vr(i)
    vvr(i) = vr(i)*vr(i)
  enddo

  call fft2(uur,uuh)
  call fft2(uvr,uvh)
  call fft2(vvr,vvh)

  do i=1,bs
    axc(i) = -ik(i)*uuh(i)-il(i)*uvh(i)+fxh(i)
    ayc(i) = -ik(i)*uvh(i)-il(i)*vvh(i)+fyh(i)
  enddo

  if (tt==1) then
    uh = uh + dt*axc
    vh = vh + dt*ayc
  else
    uh = uh + (dtl*axc - dts*axp)
    vh = vh + (dtl*ayc - dts*ayp)
  endif

  axp = axc; ayp = ayc
  end subroutine advection
end module advec

module press
use def


contains
  subroutine pressure_proj(uh,vh,p1,p2,p3)
  complex, dimension(bs), intent(inout) :: uh,vh
  real, dimension(bs), intent(in) :: p1,p2,p3  

  integer :: i
  complex :: uh_t, vh_t  

  do i=1,bs
    uh_t=uh(i); vh_t=vh(i)
    uh(i) = p1(i)*uh_t + p2(i)*vh_t
    vh(i) = p2(i)*uh_t + p3(i)*vh_t
  enddo
  end subroutine pressure_proj
end module press

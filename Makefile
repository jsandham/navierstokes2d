#Makefile for NavierStokes2D CFD code
#define variables
objects= main.o def.o init.o fft.o advec.o sgs.o press.o filt.o util.o output.o
F90= mpif90
opt= -O3 -ftree-loop-distribution #-ftree-parallelize-loops=1
execname=main
ARCH      := $(shell uname)  #ifeq ($(ARCH),Linux)
FFTW = /usr/local
NCDF = /usr/local
#MPIP = /home/james/mpiP-3.4

FFTWLIB = -L$(FFTW)/lib -lfftw3f -lm
FFTWINC = -I$(FFTW)/include
NCDFLIB = -L$(NCDF)/lib -lnetcdff -lnetcdf
NCDFINC = -I$(NCDF)/include
#MPIPLIB    = -L$(MPIP)/lib -lmpiP -lm -lunwind


#compile
$(execname): $(objects)
	$(F90) $(opt) -g -o $(execname) $(FFTWINC) $(NCDFINC) $(objects) $(FFTWLIB) $(NCDFLIB)

def.mod: def.o def.f90
	$(F90) $(opt) -g -c def.f90 
init.mod: init.o init.f90
	$(F90) $(opt) -g -c init.f90
fft.mod: fft.o fft.f90
	$(F90) $(opt) -g -c $(FFTWINC) fft.f90
advec.mod: advec.o advec.f90
	$(F90) $(opt) -g -c advec.f90
sgs.mod: sgs.o sgs.f90
	$(F90) $(opt) -g -c sgs.f90
press.mod: press.o press.f90
	$(F90) $(opt) -g -c press.f90
filt.mod: filt.o filt.f90
	$(F90) $(opt) -g -c filt.f90
util.mod: util.o util.f90
	$(F90) $(opt) -g -c $(FFTWINC) util.f90
output.mod: output.o output.f90
	$(F90) $(opt) -g -c $(NCDFINC) output.f90


def.o: def.f90
	$(F90) $(opt) -g -c def.f90
init.o: def.mod init.f90
	$(F90) $(opt) -g -c init.f90
fft.o: def.mod fft.f90
	$(F90) $(opt) -g -g -c $(FFTWINC) fft.f90
advec.o: def.mod fft.mod advec.f90
	$(F90) $(opt) -g -c advec.f90
sgs.o: def.mod fft.mod sgs.f90
	$(F90) $(opt) -g -c sgs.f90
press.o: def.mod press.f90
	$(F90) $(opt) -g -c press.f90
filt.o: def.mod filt.f90
	$(F90) $(opt) -g -c filt.f90
util.o: def.mod fft.mod util.f90
	$(F90) $(opt) -g -c $(FFTWINC) util.f90
output.o: def.mod output.f90
	$(F90) $(opt) -g -c $(NCDFINC) output.f90
main.o: def.mod init.mod fft.mod advec.mod sgs.mod press.mod filt.mod output.mod util.mod main.f90
	$(F90) $(opt) -g -c $(FFTWINC) $(NCDFINC)  main.f90 $(FFTWLIB) $(NCDFLIB)


#clean Makefile
clean:
	rm def.mod
	rm init.mod
	rm fft.mod
	rm advec.mod
	rm sgs.mod
	rm press.mod
	rm filt.mod
	rm util.mod
	rm output.mod
	rm $(objects)
#end of Makefile


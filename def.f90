!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NavierStokes2D 
! def.f90 module
! James Sandham 
! 4 Jan 2014
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! n - number of grid points in the x direction
! n - number of grid points in the y direction
! Lx - domain size in x direction
! Ly - domain size in y direction
! T - total simulation time
! dt - timestep size
! nt - number of time steps
! plot_freq - plotting frequency
! filename - output video file name


module def

  integer, parameter :: root=0
  integer :: ierr,rank,nproc,tt,ind

  real, parameter :: pi = 3.1415927
  complex, parameter :: ii = (0.0,1.0)

  !character(len=1024) :: filename='project'
  character(len=4) :: run_type='DNS'
  integer :: plot_freq = 500

  integer, parameter :: n = 512
  integer, parameter :: bs = 512*256  !n*n/nproc
  integer, parameter :: nt = 10000
  real, parameter :: n2i = 1.0/(n*n)
  real, parameter :: Lx = 2*pi
  real, parameter :: Ly = 2*pi
  real, parameter :: dx = Lx/n
  real, parameter :: dy = Ly/n
  real, parameter :: T = 1
  real, parameter :: dt = 0.0025*(512.0/n)
  real, parameter :: dts = 0.5*dt
  real, parameter :: dtl = 1.5*dt
  real, parameter :: theta = 0.5

  real, parameter :: Re=(3*n/8)**2/(4*pi)
  real, parameter :: c = 0.5
  real, parameter :: hyper=1
  real, parameter :: cs = 0.4
  real, parameter :: cs2 = (cs*dx)**2
  real, parameter :: kc = (2*pi/Lx)*(n/3)

  real :: tstart=0.0
  real :: tfinish=0.0
  real :: telapsed=0.0

end module def
